# README #

Trabalho (JSF + JPA + Postgres) de Programação 2 - UDESC/CEAVI.

### SQL ###

```SQL

-- Database: lan_house

-- DROP DATABASE lan_house;

CREATE DATABASE trabalho_final
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Portuguese_Brazil.1252'
       LC_CTYPE = 'Portuguese_Brazil.1252'
       CONNECTION LIMIT = -1;

-- Table: categoria

-- DROP TABLE categoria;

CREATE TABLE aluno
(
  id_aluno serial NOT NULL,
  nome_aluno character varying(100) NOT NULL,
  CONSTRAINT aluno_pkey PRIMARY KEY (id_aluno)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE aluno
  OWNER TO postgres;

```